%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: HardJackShootMask
  m_Mask: 00000000010000000000000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: CATRigLArmIKTarget
    m_Weight: 1
  - m_Path: CATRigRArmIKTarget
    m_Weight: 1
  - m_Path: Hard-Jack
    m_Weight: 1
  - m_Path: Rotador
    m_Weight: 1
  - m_Path: Rotador/Hard Jack
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Foot Controller L
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Foot Controller R
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/Arm Rotor
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit11
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit11/CATRigLArmDigit12
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit21
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit21/CATRigLArmDigit22
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit21/CATRigLArmDigit22/CATRigLArmDigit23
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit31
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit31/CATRigLArmDigit32
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit31/CATRigLArmDigit32/CATRigLArmDigit33
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit41
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit41/CATRigLArmDigit42
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/L
      Shoulder/L Arm/L Arm 2/L Hand/CATRigLArmDigit41/CATRigLArmDigit42/CATRigLArmDigit43
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/Neck
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/Neck/Head
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit11
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit11/CATRigRArmDigit12
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit21
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit21/CATRigRArmDigit22
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit21/CATRigRArmDigit22/CATRigRArmDigit23
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit31
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit31/CATRigRArmDigit32
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit31/CATRigRArmDigit32/CATRigRArmDigit33
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit41
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit41/CATRigRArmDigit42
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/CATRigRArmDigit41/CATRigRArmDigit42/CATRigRArmDigit43
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/R Arm/R Arm 2/R Hand/Hard-Cannon
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/CATRigSpine1/CATRigSpine2/CATRigSpine3/Torax/R
      Shoulder/Shoulder Pad 1
    m_Weight: 1
  - m_Path: Rotador/Hard Jack/Pelvis/L Leg 1
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/L Leg 1/L Leg2
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/L Leg 1/L Leg2/Knee pad L
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/L Leg 1/L Leg2/L Foot
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/L Leg 1/L Leg2/L Foot/CATRigLLegDigit11
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/R Leg 1
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/R Leg 1/R Leg 2
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/R Leg 1/R Leg 2/Knee Pad R
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/R Leg 1/R Leg 2/R Foot
    m_Weight: 0
  - m_Path: Rotador/Hard Jack/Pelvis/R Leg 1/R Leg 2/R Foot/CATRigRLegDigit11
    m_Weight: 0
