﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandWorn : MonoBehaviour
{

    Rigidbody wornRB;
    public float Speed;
    public float Height;
    public float HeightSpeed;
    float time = 0;
    public Boundary boundary;
    float y;
    float horizontalSpeed;
    Vector3 horizontalMove;

    // Use this for initialization
    void Start()
    {
        wornRB = GetComponent<Rigidbody>();
        horizontalSpeed = Speed;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    void Movement()
    {
        time += Time.deltaTime * Speed;
        y = Mathf.Sin(time) * HeightSpeed;

        if (wornRB.position.x <= boundary.xMin)
        {
            horizontalSpeed = Speed;
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (wornRB.position.x >= boundary.xMax)
        {
            horizontalSpeed = -Speed;
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        horizontalMove.Set(horizontalSpeed, y, 0);

        wornRB.velocity = horizontalMove;
    }
}
