﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int Damage;
    public string TagToFind;

    Rigidbody bulletRB;
    public float Speed;

    public GameObject PlayerBulletExplosion;
    public GameObject EnemyBulletExplosion;

    // Use this for initialization
    void Awake()
    {
        bulletRB = GetComponent<Rigidbody>();

    }

    public void Shoot()
    {
        bulletRB.velocity = transform.right * Speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(TagToFind))
        {
            SetState(false);
            if(TagToFind == "Enemy")
            {
                Instantiate(PlayerBulletExplosion, transform.position, transform.rotation);
                other.GetComponent<EnemyHealthManager>().TakeDamage(Damage);
            }
            else if(TagToFind == "Player")
            {
                Instantiate(EnemyBulletExplosion, transform.position, transform.rotation);
                other.GetComponent<PlayerHealthManager>().TakeDamage(Damage, false);
            }
        }
        if (other.CompareTag("Level"))
        {
            SetState(false);
        }
    }

    void SetState(bool state)
    {
        gameObject.SetActive(state);
    }
}
