﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float Speed;
    public float JumpSpeed;
    public float Gravity;
    public float MaxGravity;
    public float KnockBackForce;
    float jumpCounter;
    private Vector3 moveDirection = Vector3.zero;
    CharacterController playerController;

    public Transform BulletEmitterFront;
    float horizontalAx;
    int PooledBullets = 10;
    List<GameObject> bullets;
    GameObject BulletsFather;
    public GameObject Bullet;
    int currentBullet;
    float fireRate = 0.25f;
    float nextFire = 0.5f;

    bool canDoubleJump;
    bool doublejump;

    bool knockbaqueando;
    bool knockBackRight;

    public Animator playerAnim;
    GameController theGameController;
    AudioSource shotAudio;
    public AudioSource powerUp;

    void Start()
    {
        playerController = GetComponent<CharacterController>();

        BulletsFather = new GameObject("BulletsFather");
        bullets = new List<GameObject>();
        GameObject obj;
        for (int i = 0; i < PooledBullets; i++)
        {
            obj = (GameObject)Instantiate(Bullet);
            obj.SetActive(false);
            bullets.Add(obj);
            obj.transform.SetParent(BulletsFather.transform);
        }

        playerAnim = GetComponent<Animator>();

        theGameController = FindObjectOfType<GameController>();

        shotAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (knockbaqueando == true)
        {
            return;
        }

        if (!theGameController.gameOver)
        {
            Movement();
            Jump();
            Shooting();
        }

        horizontalAx = Input.GetAxis("Horizontal");
    }

    void Movement()
    {
        moveDirection.x = Input.GetAxis("Horizontal") * Speed;

        playerAnim.SetFloat("XAxis", Mathf.Abs(moveDirection.x));
        playerController.Move(moveDirection * Time.deltaTime);

        if (moveDirection.x > 0)
        {
            transform.eulerAngles = new Vector3(0, 90, 0);
            knockBackRight = true;
        }
        else if (moveDirection.x < 0)
        {
            transform.eulerAngles = new Vector3(0, 270, 0);
            knockBackRight = false;
        }
    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump") && playerController.isGrounded)
            jumpCounter = 0;

        if (Input.GetButtonDown("Jump") && !playerController.isGrounded && canDoubleJump && !doublejump)
        {
            jumpCounter = 0;
            doublejump = true;
        }

        if (Input.GetButton("Jump") && jumpCounter < 0.5f)
        {
            moveDirection.y = JumpSpeed;
            jumpCounter += Time.deltaTime;
        }
        else
        {
            moveDirection.y -= Gravity * Time.deltaTime;
            if (moveDirection.y < -MaxGravity)
            {
                moveDirection.y = -MaxGravity;
            }
            if (playerController.isGrounded)
            {
                moveDirection.y = -5;
                doublejump = false;
            }
        }

        if (Input.GetButtonUp("Jump"))
            jumpCounter = 1;

        playerAnim.SetFloat("YAxis", Mathf.Abs(jumpCounter));
        playerAnim.SetBool("IsGrounded", playerController.isGrounded);
    }
    public void DoubleJump(bool gotJetPack)
    {
        if (gotJetPack)
        {
            powerUp.Play();
            canDoubleJump = gotJetPack;
        }
    }

    void Shooting()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            if (horizontalAx != 0 || horizontalAx <= 0)
            {
                bullets[currentBullet].transform.position = BulletEmitterFront.position;
                bullets[currentBullet].transform.rotation = BulletEmitterFront.rotation;
            }
            bullets[currentBullet].GetComponent<Bullet>().Shoot();
            bullets[currentBullet].SetActive(true);
            currentBullet++;
            if (currentBullet >= bullets.Count)
            {
                currentBullet = 0;
            }
            shotAudio.Play();
        }
        playerAnim.SetBool("Shooting", Input.GetButton("Fire1"));
    }

    public IEnumerator KnockOut()
    {
        float t = 0;
        float xDirection;
        knockbaqueando = true;

        while (t < 0.5f)
        {
            moveDirection.y -= Gravity * Time.deltaTime;
            if (moveDirection.y < -MaxGravity)
            {
                moveDirection.y = -MaxGravity;
            }

            if (knockBackRight)
                xDirection = -KnockBackForce;
            else
                xDirection = KnockBackForce;

            moveDirection.Set(xDirection, moveDirection.y, 0);

            playerController.Move(moveDirection * Time.deltaTime);
            t += Time.deltaTime;
            yield return null;
        }
        knockbaqueando = false;
    }
}
