﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingAlien : MonoBehaviour
{
    Rigidbody alienRB;
    public float Speed;
    public float Height;
    public float HeightSpeed;
    float time = 0;
    //RestrictionZone
    public Boundary boundary;
    float y;
    float horizontalSpeed;
    Vector3 horizontalMove;
    //Attack
    bool playerInTheZone;
    public Transform Player;
    float distance;
    Vector3 positionBeforeAttack;
    bool attacking;
    public float attackSpeed;

    // Use this for initialization
    void Start()
    {
        alienRB = GetComponent<Rigidbody>();
        horizontalSpeed = Speed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!attacking)
        {
            if (playerInTheZone)
            {
                StartCoroutine(Attack());
            }
            else
            {
                Movement();
            }
        }
        PlayerDetection();
    }

    void Movement()
    {
        time += Time.deltaTime * Speed;
        y = Mathf.Sin(time) * HeightSpeed;

        if (alienRB.position.x <= boundary.xMin)
        {
            horizontalSpeed = Speed;
        }
        if (alienRB.position.x >= boundary.xMax)
        {
            horizontalSpeed = -Speed;
        }

        horizontalMove.Set(horizontalSpeed, y, 0);

        alienRB.velocity = horizontalMove;
    }

    void PlayerDetection()
    {
        if (Player)
        {
            distance = Vector3.Distance(Player.position, transform.position);
        }

        if (distance < 5)
        {
            playerInTheZone = true;
        }
        else if (distance > 6)
        {
            playerInTheZone = false;
        }
    }

    IEnumerator Attack()
    {
        attacking = true;
        positionBeforeAttack = transform.position;
        float t = 0;
        while (t < 1)
        {
            transform.position = Vector3.Lerp(transform.position, Player.position, t);
            yield return null;
            t += Time.deltaTime * attackSpeed;
        }
        t = 0;
        while (t < 1)
        {
            transform.position = Vector3.Lerp(transform.position, positionBeforeAttack, t);
            yield return null;
            t += Time.deltaTime * attackSpeed;
        }
        attacking = false;
        yield return new WaitForSeconds(1);
    }
}
