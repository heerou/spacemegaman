﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZones : MonoBehaviour
{
    public bool zoneTouched;
    public int Damage;

    private void Start()
    {
        zoneTouched = false;
    }    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            zoneTouched = true;
            other.GetComponent<PlayerHealthManager>().TakeDamage(Damage, zoneTouched);
        }
        zoneTouched = false;
    }
}
