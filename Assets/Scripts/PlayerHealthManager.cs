﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthManager : MonoBehaviour
{
    public int PlayerMaxHealth;
    int playerCurrentHealth;
    bool isPlayerAlive;
    GameController gameController;
    GameObject gameControllerObj;

    public Slider HealthBar;

    Player thePlayer;

    bool invincible;
    Renderer[] playerRend;

    int RegenHealth;
    public AudioSource powerUp;

    // Use this for initialization
    void Start()
    {
        isPlayerAlive = true;
        playerCurrentHealth = PlayerMaxHealth;


        HealthBar.maxValue = PlayerMaxHealth;
        HealthBar.value = playerCurrentHealth;

        gameControllerObj = GameObject.FindGameObjectWithTag("GameController");

        if (gameControllerObj != null)
        {
            gameController = gameControllerObj.GetComponent<GameController>();
        }
        else
        {
            Debug.Log("No hay GameController");
        }

        thePlayer = GetComponent<Player>();
        playerRend = GetComponentsInChildren<Renderer>();
        for (int i = 1; i < playerRend.Length; i++)
        {
            playerRend[i].enabled = true;
        }
    }

    public virtual void TakeDamage(int damage, bool zoneTouched)
    {
        if (invincible == true && !zoneTouched)
        {
            return;
        }
        playerCurrentHealth -= damage;
        HealthBar.value = playerCurrentHealth;

        StartCoroutine(thePlayer.KnockOut());

        if (playerCurrentHealth < 1)
        {
            isPlayerAlive = false;
            gameController.GameOver(isPlayerAlive);
        }
        else
        {
            StartCoroutine(PlayerInvincible());
        }

    }

    public IEnumerator PlayerInvincible()
    {
        invincible = true;
        for (int i = 1; i < 8; i++)
        {
            for (int j = 1; j < playerRend.Length; j++)
            {
                playerRend[j].enabled = false;
                yield return new WaitForSeconds(0.1f);
                playerRend[j].enabled = true;
                yield return new WaitForSeconds(0.1f);
            }
        }
        invincible = false;
    }

    public void RestoreHealth(bool restore, int regenHP)
    {
        if (restore)
        {
            powerUp.Play();
            RegenHealth = regenHP;
            playerCurrentHealth += RegenHealth;
            HealthBar.value = playerCurrentHealth;
            if (playerCurrentHealth > PlayerMaxHealth)
            {
                playerCurrentHealth = PlayerMaxHealth;
                RegenHealth = 0;
            }
        }
    }
}
