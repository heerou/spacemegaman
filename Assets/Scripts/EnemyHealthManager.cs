﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour
{

    public int EnemyMaxHealth;
    int enemyCurrentHealth;
    public bool EnemyDeath;    

    private void Start()
    {
        EnemyDeath = false;
        enemyCurrentHealth = EnemyMaxHealth;
    }

    public virtual void TakeDamage(int damage)
    {
        enemyCurrentHealth -= damage;

        if (enemyCurrentHealth < 1)
        {
            EnemyDeath = true;
            gameObject.SetActive(false);
        }
    }
}
