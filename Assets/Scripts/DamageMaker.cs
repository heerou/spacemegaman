﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageMaker : MonoBehaviour
{

    public int Damage;
    public string TagToFind;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(TagToFind))
        {
            other.GetComponent<PlayerHealthManager>().TakeDamage(Damage, false);
        }
    }
}
