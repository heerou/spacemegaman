﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{

    Rigidbody bossRB;
    public float Speed;
    public float Height;
    public float HeightSpeed;
    public float DownSpeed;
    float time = 0;
    public Boundary boundary;
    float y;
    float horizontalSpeed;
    Vector3 horizontalMove;
    FollowPlayer bossRoom;
    int randomAttack = 0;
    EnemyHealthManager hp;
    public bool AttackTime;

    //shoting
    public Bullet Bullet;
    public Transform ShotSpawn;
    float fireRate = 0.5f;
    float nextFire = 0.5f;
    int currentBullet;
    public int PooledBullets = 10;
    List<Bullet> bullets;
    GameObject fatherBossBullets;
    
    AudioSource shotAudio;    

    // Use this for initialization
    void Start()
    {
        bossRB = GetComponent<Rigidbody>();
        horizontalSpeed = Speed;
        hp = GetComponent<EnemyHealthManager>();
        AttackTime = false;
        StartCoroutine(PhaseManager());

        bullets = new List<Bullet>();
        fatherBossBullets = new GameObject("BossBullets");
        Bullet obj;
        for (int i = 0; i <= PooledBullets; i++)
        {
            obj = Instantiate(Bullet);
            obj.gameObject.SetActive(false);
            bullets.Add(obj);
            obj.transform.SetParent(fatherBossBullets.transform);
        }
        shotAudio = GetComponent<AudioSource>();
    }

    IEnumerator PhaseManager()
    {
        while (!hp.EnemyDeath)
        {
            yield return Movement();

            randomAttack = Random.Range(0, 100);
            if (randomAttack < 30)
            {
                AttackTime = true;
                yield return Attack();
                AttackTime = false;
            }
        }
    }

    IEnumerator Movement()
    {
        time += Time.deltaTime * Speed;
        y = Mathf.Sin(time) * HeightSpeed;

        if (bossRB.position.x <= boundary.xMin)
        {
            horizontalSpeed = Speed;
        }
        if (bossRB.position.x >= boundary.xMax)
        {
            horizontalSpeed = -Speed;
        }
        horizontalMove.Set(horizontalSpeed, y, 0);
        bossRB.velocity = horizontalMove;
        yield return null;
    }

    IEnumerator Attack()
    {
        if (AttackTime)
        {
            if (bossRB.position.x <= 53 || bossRB.position.x < 56)
            {
                bossRB.velocity = Vector3.zero;
                yield return Mover(transform, new Vector3(bossRB.position.x, -50, 0), DownSpeed * Time.deltaTime);
                Shooting();
                yield return Mover(transform, new Vector3(bossRB.position.x, -40, 0), DownSpeed * Time.deltaTime);
                AttackTime = false;
            }
        }
    }

    IEnumerator Mover(Transform transitionObject, Vector3 finalPos, float speed)
    {
        float t = 0;
        Vector3 initialPos = transitionObject.position;
        while (t < 1)
        {
            transitionObject.position = Vector3.Lerp(initialPos, finalPos, t);
            t += Time.deltaTime * speed;
            yield return null;
        }
        transitionObject.position = Vector3.Lerp(initialPos, finalPos, 1);
    }

    void Shooting()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            bullets[currentBullet].transform.position = ShotSpawn.position;
            bullets[currentBullet].transform.rotation = ShotSpawn.rotation;
            bullets[currentBullet].gameObject.SetActive(true);
            bullets[currentBullet].Shoot();
            shotAudio.Play();
        }
        currentBullet++;
        if (currentBullet >= bullets.Count)
        {
            currentBullet = 0;
        }
    }    
}
