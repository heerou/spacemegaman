﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform PlayerTrans;
    public float Smooth;
    Vector3 Distance;
    bool moveCamera;
    bool bossFight;
    float xAxis = 0;
    float yAxis = 0;
    public BossScript TheBoss;
    public Barriers theBarrier;

    void Start()
    {
        Distance.z = -10;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (moveCamera)
        {
            Vector3 newPos = new Vector3(PlayerTrans.position.x + xAxis, yAxis, 0) + Distance;
            transform.position = Vector3.Lerp(transform.position, newPos, Smooth * Time.deltaTime);
        }
        if (bossFight)
        {
            moveCamera = false;
            Vector3 newPosBoss = new Vector3(xAxis, yAxis, 0) + Distance;
            transform.position = Vector3.Lerp(transform.position, newPosBoss, Smooth * Time.deltaTime);
        }
    }

    public void MoveCamera(bool triggered, float x, float y, bool bossRoom)
    {
        moveCamera = triggered;
        xAxis = x;
        yAxis = y;
        bossFight = bossRoom;
        if(bossRoom == true)
        {
            TheBoss.gameObject.SetActive(true);
            theBarrier.gameObject.SetActive(true);
        }
    }
}
