﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryMenu : MonoBehaviour {

    public string QuitLevelName;
    public GameController gameController;
    Scene theScene;

    // Use this for initialization
    void Start()
    {
        theScene = SceneManager.GetActiveScene();
    }

    public void RestartLevel()
    {
        if (gameController.gameOver)
        {
            SceneManager.LoadScene(theScene.name);
        }
    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene(QuitLevelName);
    }
}
