﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDeath : MonoBehaviour
{
    public EnemyHealthManager BossHealthManager;
    public BossScript TheBoss;
    public bool BossDies;
    public AudioSource explosionAudio;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (BossHealthManager.EnemyDeath && TheBoss.gameObject.activeInHierarchy == false)
        {
            BossDies = true;
        }
    }
}
