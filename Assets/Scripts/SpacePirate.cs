﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpacePirate : MonoBehaviour
{

    Rigidbody SPirateRB;
    public float Speed;
    public Boundary boundary;
    float time = 0;
    float horizontalSpeed;
    Vector3 horizontalMove;

    Ray ray;
    RaycastHit rayHit;
    public float MaxRayDistance;
    int rayMask;

    bool playerViewed;
    public Bullet Bullet;
    public Transform ShotSpawn;
    float fireRate = 0.5f;
    float nextFire = 0.5f;
    int currentBullet;
    public int PooledBullets = 10;
    List<Bullet> bullets;
    GameObject fatherEnemyBullets;

    AudioSource shotAudio;

    // Use this for initialization
    void Start()
    {
        SPirateRB = GetComponent<Rigidbody>();
        horizontalSpeed = Speed;
        rayMask = LayerMask.GetMask("Player");

        playerViewed = false;
        bullets = new List<Bullet>();
        fatherEnemyBullets = new GameObject("EnemyBullets");
        Bullet obj;
        for (int i = 0; i < PooledBullets; i++)
        {
            obj = Instantiate(Bullet);
            obj.gameObject.SetActive(false);
            bullets.Add(obj);
            obj.transform.SetParent(fatherEnemyBullets.transform);
        }

        shotAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DetectPlayer();
        if (playerViewed)
        {
            Shooting();
            SPirateRB.velocity = Vector3.zero;
        }
        else
        {
            Movement();
        }
    }

    void Movement()
    {
        time += Time.deltaTime * Speed;

        if (SPirateRB.position.x <= boundary.xMin)
        {
            horizontalSpeed = Speed;
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (SPirateRB.position.x >= boundary.xMax)
        {
            horizontalSpeed = -Speed;
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        horizontalMove.Set(horizontalSpeed, 0, 0);

        SPirateRB.velocity = horizontalMove;
    }

    void DetectPlayer()
    {
        //ray = new Ray(transform.position, transform.right * MaxRayDistance);
        Debug.DrawRay(transform.position + Vector3.up, transform.right * MaxRayDistance, Color.red);

        if (Physics.Raycast(transform.position + Vector3.up, transform.right, MaxRayDistance, rayMask))
        {
            playerViewed = true;
        }
        else
        {
            playerViewed = false;
        }
    }

    void Shooting()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            bullets[currentBullet].transform.position = ShotSpawn.position;
            bullets[currentBullet].transform.rotation = ShotSpawn.rotation;
            bullets[currentBullet].gameObject.SetActive(true);
            bullets[currentBullet].Shoot();
            shotAudio.Play();
        }
        currentBullet++;
        if (currentBullet >= bullets.Count)
        {
            currentBullet = 0;
        }
    }
}
