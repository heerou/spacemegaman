﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    public bool gameOver;
    public Player ThePlayer;
    public DeathMenu theDeathMenu;
    public PauseMenu thePauseMenu;
    public VictoryMenu theVictoryMenu;
    int pauseCounter;

    Player thePlayerAnim;

    public BossDeath BossDeathScript;

    EventSystem myEventSystem;
    public GameObject DeathMenuBtn;
    public GameObject PauseMenuBtn;
    public GameObject VictoryMenuBtn;

    bool gameOverOnce;
    bool gameBeaten;

    private void OnEnable()
    {
        myEventSystem = EventSystem.current;
    }

    // Use this for initialization
    void Start()
    {
        gameOver = false;
        pauseCounter = 0;
        thePlayerAnim = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver && !gameOverOnce)
        {
            gameOverOnce = true;
            thePlayerAnim.playerAnim.SetBool("Death", gameOver);
            theDeathMenu.gameObject.SetActive(true);
            myEventSystem.SetSelectedGameObject(DeathMenuBtn);
        }

        if (Input.GetButtonDown("Pause"))
        {
            thePauseMenu.gameObject.SetActive(true);
            myEventSystem.SetSelectedGameObject(PauseMenuBtn);
            pauseCounter += 1;
            Time.timeScale = 0f;
            if (pauseCounter > 1)
            {
                thePauseMenu.gameObject.SetActive(false);
                Time.timeScale = 1f;
                pauseCounter = 0;
            }
        }

        if (BossDeathScript.BossDies && !gameBeaten)
        {
            gameBeaten = true;
            theVictoryMenu.gameObject.SetActive(true);
            myEventSystem.SetSelectedGameObject(VictoryMenuBtn);
        }
    }

    public void GameOver(bool isPlayerAlive)
    {
        if (!isPlayerAlive)
        {
            gameOver = true;
        }
    }
}
