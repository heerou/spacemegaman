﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraTrigger : MonoBehaviour
{
    public FollowPlayer CameraObj;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "TriggerBeginning")
        {
            CameraObj.MoveCamera(true, 3, 3, false);
        }
        if (other.gameObject.name == "Trigger1")
        {
            CameraObj.MoveCamera(true, 2, 8, false);
        }
        if (other.gameObject.name == "Trigger2")
        {
            CameraObj.MoveCamera(true, 0, -1, false);
        }
        if (other.gameObject.name == "Trigger3")
        {
            CameraObj.MoveCamera(true, 0, -10, false);
        }
        if (other.gameObject.name == "Trigger4")
        {
            CameraObj.MoveCamera(true, 0, -15, false);
        }
        if (other.gameObject.name == "Trigger5")
        {
            CameraObj.MoveCamera(true, 0, -26, false);
        }
        if (other.gameObject.name == "Trigger6")
        {
            CameraObj.MoveCamera(true, 0, -41, false);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Trigger2")
        {
            CameraObj.MoveCamera(true, 0, 3, false);
        }
        if (other.gameObject.name == "Trigger1")
        {
            CameraObj.MoveCamera(true, 0, 3, false);
        }
        if (other.gameObject.name == "Trigger4")
        {
            CameraObj.MoveCamera(true, 0, -10, false);
        }
        if (other.gameObject.name == "Trigger7")
        {
            CameraObj.MoveCamera(true, 64f, -46.5f, true);
        }
    }
}
