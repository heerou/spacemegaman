﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{
    public string QuitLevelName;
    public GameController gameController;
    Scene theScene;

    private void Start()
    {
        theScene = SceneManager.GetActiveScene();
    }

    public void RestartLevel()
    {
        if (gameController.gameOver)
        {
            SceneManager.LoadScene(theScene.name);
        }
    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene(QuitLevelName);
    }
}
