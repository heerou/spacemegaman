﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetPack : MonoBehaviour
{
    public Player player;
    AudioSource powerUp;

    private void Update()
    {
        transform.Rotate(0, 100 * Time.deltaTime, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {            
            player.DoubleJump(true);
            transform.gameObject.SetActive(false);
        }
    }
}
