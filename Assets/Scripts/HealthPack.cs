﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour
{
    public PlayerHealthManager thePlayerHealthManager;
    public int RegenHealth;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 100 * Time.deltaTime, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            thePlayerHealthManager.RestoreHealth(true, RegenHealth);
            transform.gameObject.SetActive(false);
        }
    }
}
