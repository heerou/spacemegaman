# SpaceMegaman

Test of 2D action scroller in unity 2017.3.0f3

Models and special help provided by [Rival Arts Studio](https://rivalartstudio.com/)_
Intro Screen Song: [Instro Screen](https://dl-sounds.com/royalty-free/intro-screen/)_
Level Song by kingMeteor: [Titania (2)](https://vgmusic.com/file/ca6f7d596f925027bd3888f769990e69.html)_
Shot Effect by fins: [shot 1](https://freesound.org/people/fins/sounds/368734/)_